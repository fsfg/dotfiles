function mkcd
    if test (count "$argv") -ne 1
        echo "usage: mkcd <dir>"
    else
        mkdir -p "$argv[1]" && cd "$argv[1]"
    end
end
