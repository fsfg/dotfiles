function fish_greeting
    set_color brblack
    printf ">> %s\n" (date '+%a, %b %-d | %-H:%M')
    set_color normal
end
