function fish_right_prompt

    # Write pipestatus
    set -l last_pipestatus $pipestatus

    set -l prompt_status (
        __fish_print_pipestatus \
            '['\
            ']'\
            '|'\
            (set_color "$fish_color_status") \
            (set_color --bold "$fish_color_status") \
            $last_pipestatus
        )
    echo -n -s $prompt_status

    test $prompt_status
    and echo -n -s ' | '


    # Python virtual environment
    if not set -q VIRTUAL_ENV_DISABLE_PROMPT
        set -g VIRTUAL_ENV_DISABLE_PROMPT true
    end
    if test "$VIRTUAL_ENV"
        set -l col (set_color "blue")
        set -l venv (basename (dirname "$VIRTUAL_ENV"))
        set -l normal (set_color "normal")
        printf "%s" "(venv " "$col" "$venv" "$normal" ")"
    end

end
