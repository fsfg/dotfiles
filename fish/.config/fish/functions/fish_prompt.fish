function fish_prompt --description 'Write out the prompt'

    function _prompt_wrapper
        set -l retc1 "$argv[1]"
        set -l field_name "$argv[2]"
        set -l retc2 "$argv[3]"
        set -l field_value "$argv[4]"

        if [ $field_value ]
            printf " %s" '('

            test -n "$retc1" ; and set_color "$retc1"
            test -n "$field_name" ; and printf "%s " "$field_name"
            set_color "normal"

            test -n "$retc2" ; and set_color "$retc2"
            printf "%s" "$field_value"
            set_color "normal"

            printf "%s " ')'
        end
    end

    set -l normal (set_color "normal")

    # Color the prompt differently when we're root
    set -l color_cwd "$fish_color_cwd"
    set -l suffix '> '

    if contains -- $USER 'root' 'toor'
        if set -q fish_color_cwd_root
            set color_cwd "$fish_color_cwd_root"
        end
        set suffix '# '
    end


    # Prompt string
    ## Current directory
    ### If we're running via SSH, display the host.
    if set -q SSH_TTY
        printf "%s" (set_color "fish_color_host") (prompt_hostname) "$normal"
    end
    printf "%s" (tput setab 2) (set_color "black") '  ' (prompt_pwd) ' ' "$normal"

    ## Git status if available
    set -l _git_value (fish_git_prompt | string trim -c ' ()')
    printf "%s" (_prompt_wrapper 'green' '' '' "$_git_value")

    ## Prompt actually
    echo
    printf "%s" (set_color --bold $color_cwd) "$suffix" "$normal"
end
