# Autoload
fzf_key_bindings
. /usr/share/autojump/autojump.fish
source /opt/asdf-vm/asdf.fish

# Aliases
alias grep="grep --color=auto -i"
alias la="ls --almost-all --classify --color=auto --group-directories-first \
            --human-readable -l --literal"
alias lsd="ls --color=auto --human-readable --indicator-style=none -l \
            --literal -d */"
alias lad="ls --almost-all --color=auto --human-readable \
            -l --literal --indicator-style=none -d */"
alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -I"
alias ..="cd .."
alias ...="cd ../.."

alias pip-upgrade-all="pip list --outdated --format=freeze | \
    grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U"

alias vi='nvim'
alias ex='nvim -e'
alias view='nvim -R'

alias pacman='pacman --color always'
alias yy='yay -Syu --devel'



# vim: ft=sh
