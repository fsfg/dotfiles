#!/usr/bin/env zsh


plugins=(
  '/etc/profile.d/autojump.sh'
  '/usr/share/fzf/completion.zsh'
  '/usr/share/doc/pkgfile/command-not-found.zsh'
  '/usr/share/fzf/key-bindings.zsh'
  '/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh'
#  '/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh'
#  '/opt/asdf-vm/asdf.sh'
)

for plugin in $plugins;
  do [[ -s $plugin ]] && . $plugin end
done

