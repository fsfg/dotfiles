fpath=(
#  "$XDG_CONFIG_HOME/zsh/completion"
  "$XDG_CONFIG_HOME/zsh/theme"
  $fpath
)

autoload -Uz promptinit && promptinit
prompt thetheme

HISTFILE="$XDG_DATA_HOME"/zsh/history
HISTSIZE=4096
SAVEHIST=8192
HISTORY_IGNORE="(&|cd|..|...|ls|la|lad|lsd|[bf]g|exit|yy|j)*"
setopt hist_ignore_all_dups hist_ignore_space hist_reduce_blanks hist_save_no_dups inc_append_history_time

zshaddhistory() {
  emulate -L zsh
  [[ $1 != ${~HISTORY_IGNORE} ]]
}


setopt nomatch notify correct
unsetopt autocd beep extendedglob

bindkey -e

zstyle ':completion:*' menu select

autoload -Uz compinit && compinit
autoload -U colors && colors

# Autoquote URLs
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic

unsetopt CASE_GLOB

# Load configs from $XDG_CONFIG_HOME/$1/*$2
function load_configs () {
  if [[ -z $1 ]]; then return 0; else config_dir="$1"; fi
  ext="${2:-''}"
  if test -d "$XDG_CONFIG_HOME/$config_dir"; then
    for conf in "$XDG_CONFIG_HOME/$config_dir"/*"$ext"; do
      test -r "$conf" && source "$conf";
    done
  fi
}

# PLUGINS
load_configs 'zsh' '.zsh'
# ENDOF PLUGINS


# COMMON POSIX SHELL FUNCTIONS AND ALIASES
load_configs 'common_posix_shell' '.sh'
# ENDOF COMMON POSIX SHELL FUNCTIONS AND ALIASES


# FUNCTIONS
# ENDOF FUNCTIONS


# ALIASES
# ENDOF ALIASES



# vim: filetype=zsh foldmethod=marker autoindent expandtab shiftwidth=2
