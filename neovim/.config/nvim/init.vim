" -------------
" ### Plugins (with VimPlug)
call plug#begin(stdpath('data') . '/plugged')
"Plug 'roxma/nvim-completion-manager'
"Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'
Plug 'gruvbox-community/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'bling/vim-bufferline'
"Plug 'vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'baskerville/vim-sxhkdrc'
Plug 'farmergreg/vim-lastplace'
"Plug 'Glench/Vim-Jinja2-Syntax'
"Plug 'python-mode/python-mode', { 'branch': 'develop' }
Plug 'jeffkreeftmeijer/vim-numbertoggle'
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf'
Plug 'editorconfig/editorconfig-vim'
Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': 'python3 -m chadtree deps'}
call plug#end()
" -------------


" -------------
" ### GRUVBOX ###
colorscheme gruvbox
set background=dark
"let g:gruvbox_bold = 1
let g:gruvbox_italic = 1
"let g:gruvbox_underline = 1
"let g:gruvbox_undercurl = 1
"let g:gruvbox_termcolors = 256
"let g:gruvbox_contrast_dark = 'hard'
"let g:gruvbox_contrast_light = 'soft'
let g:gruvbox_italicize_strings = 1
"let g:gruvbox_improved_strings =1
let g:gruvbox_improved_warnings = 1
" -------------


" -------------
" ### AIRLINE ###
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_detect_modified = 1
let g:airline_inactive_collapse = 1
let g:airline_powerline_fonts = 1
"let g:airline_symbols_ascii = 1
let g:airline_focuslost_inactive = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_symbols.linenr = ''
let g:airline_symbols.maxlinenr = ''

let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
" -------------


" -------------
" ### deoplete.nvim
"let g:deoplete#enable_at_startup = 1
" -------------


" -------------
" ### Copy with Ctrl+c, paste with Ctrl+v ###
"vnoremap <C-c> :w !xclip -i -sel c<CR><CR>
"noremap <C-v> :r xclip -o -sel -c<CR><CR>
" -------------

" -------------
:set tabstop=4 shiftwidth=4 expandtab
" -------------


" -------------
set clipboard=unnamedplus
set confirm
:set cursorline
:set number relativenumber
set termguicolors
set title
set undofile
" -------------


" -------------
"  Hotkeys
nnoremap <leader>v <cmd>CHADopen<cr>
nnoremap <leader>l <cmd>call setqflist([])<cr>
"nnoremap <F3> :set hlsearch!<CR>
