#!/usr/bin/sh

### Check for dependencies.  Fail if unmet.
checkdep() {
    command -v "$1" > /dev/null || { echo "Missing dependency: $1"; exit 1; }
}

checkdep dunstify

### Set brightness via xbacklight/light on Redshift status changes,
### show notification and send data to panel.
### udev rule is needed to allow user of 'video' group change brightness
### see https://wiki.archlinux.org/index.php/Screen_brightness

### Set brightness values for each period.
### Range from 1 to 100 is valid
BRIGHTNESS_DAY=99
BRIGHTNESS_TRANSITION=80
BRIGHTNESS_NIGHT=60
### Set fade time for changes to one minute (xbacklight)
# FADE_TIME=60000

# PANEL_FIFO="/tmp/panel-fifo"
# XOB_FIFO="/tmp/xob-fifo"

event="$1"
period="$3"

case "$event" in
    period-changed)
        dunstify -a "redshift" -r 999 -u low \
            "Redshift" "Period changed to ${period^^}" &

        case "$period" in
            daytime)
                brt="$BRIGHTNESS_DAY" ;;
            transition)
                brt="$BRIGHTNESS_TRANSITION" ;;
            night)
                brt="$BRIGHTNESS_NIGHT" ;;
            none)
                brt=100 ;;
        esac

        light -S "$brt" &
        # xbacklight -set "$brt" -time "$FADE_TIME" &

        printf "%s\n" "$brt" > "$XOB_FIFO" &
        printf "B%s\n" "$period" > "$PANEL_FIFO" &
esac
