# Like normal autojump when used with arguments
# but displays an fzf prompt when used without
function j () {
    if [[ "$#" -ne 0 ]]; then
        cd $(autojump $@); return
    fi

    cd "$(\
        autojump -s \
        | sort -k1gr \
        | awk '$1 ~ /[0-9]:/ && $2 ~ /^\// { for (i=2; i<=NF; i++) { print $(i) } }' \
        | fzf --height 40% --reverse --inline-info\
        )"
}


# Search for man pages with fzf
function fman () {
    man -k . | fzf --prompt='Man> ' | awk '{print $1}' | xargs -r man
}


# find file by template
function ff () {
    find . -type f -iname '*'"$*"'*' -ls ;
}


function find-back () {
    path="$(pwd)"
    while [[ $path != "$HOME" ]]; do
        find "$path" -maxdepth 1 -mindepth 1 -type f,l -iname "$1"
        # Note: if you want to ignore symlinks, use "$(realpath -s "$path"/..)"
        path="$(readlink -f "$path"/..)"
    done
}


# find string within files
function fstr () {
    OPTIND=1
    local case usage SMSO RMSO
    case=
    usage="Usage: fstr [-i] \"template\" [\"file_template\"] "
    while getopts :it opt; do
        case "$opt" in
            i)
                case="-i "
                ;;
            *)
                echo "$usage"
                return
                ;;
        esac
    done
    shift $(( OPTIND - 1 ))
    if [ "$#" -lt 1 ]; then
        echo "$usage"
        return;
    fi
    SMSO="$(tput smso)"
    RMSO="$(tput rmso)"
    find . -type f -name "${2:-*}" -print0 | \
        xargs -0 grep -sn "${case}" "$1" 2>&- | \
        sed "s/$1/${SMSO}\0${RMSO}/gI" | \
        more
}


function wan_ip () {
    curl -s "checkip.dyndns.com" | awk '{print $6}' | sed 's/<.*>//'
}


function weather () {
    clear
    curl "v2.wttr.in/$1"
}


function transfer () {
    local tmpfile
    if [ $# -eq 0 ]; then
        echo -e "No arguments specified. \
            Usage:\ntransfer /tmp/test.md\ncat /tmp/test.md | \
            transfer test.md";
        return 1;
    fi

    tmpfile=$( mktemp -t transferXXX );

    if tty -s; then
        basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g');
        curl --progress-bar --upload-file \
            "$1" "https://transfer.sh/$basefile" >> "$tmpfile";
    else
        curl --progress-bar --upload-file \
            "-" "https://transfer.sh/$1" >> "$tmpfile" ;
    fi;

    cat "$tmpfile";
    rm -f "$tmpfile";
}



# vim: ft=sh
