alias grep='grep --color=auto -i'

common_ls_params='--color --human-readable -l --literal -v'
alias ls="command ls ${common_ls_params} --classify --group-directories-first"
alias la="command ls ${common_ls_params} --classify --group-directories-first --almost-all"
alias lsd="command ls ${common_ls_params} --directory -- *(/)"
#alias lad="command ls ${common_ls_params} --directory --almost-all -- *(/) .[^.]*(/)"
alias lad='tree -adli -L 1 --noreport -pughDFv'

alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -I'
alias ..='cd ..'
alias ...='cd ../..'

alias pip-upgrade-all="pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U"

alias vi='nvim'
alias ex='nvim -e'
alias view='nvim -R'

alias pacman='pacman --color always'
alias yy='yay -Syu --devel'

alias sqlite3='sqlite3 -init "$XDG_CONFIG_HOME"/sqlite3/sqliterc'
alias svn='svn --config-dir "$XDG_CONFIG_HOME"/subversion'

# alias wget='wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
# alias startx='startx "$XDG_CONFIG_HOME/X11/xinitrc" -- "$XDG_CONFIG_HOME/X11/xserverrc" vt1'

#alias cmus='tmux new-session -A -D -s cmus "$(which cmus)"'

### Find directories by their name
# alias fd='find . -type d -name'
### Find files by their name
# alias ff='find . -type f -name'
