lbin=$HOME/.local/bin
export PATH=$lbin/scripts:$lbin:$PATH

export MONITOR=LVDS-0

export EDITOR="nvim"
export PAGER="/usr/bin/most -s -w"
export VISUAL="nvim -R"

### XDG Base Directory Specification
export XDG_CACHE_HOME=$HOME/.cache
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
# export XDG_CONFIG_DIRS=/etc/xdg
# export XDG_DATA_DIRS=/usr/local/share/:/usr/share/
# export XDG_RUNTIME_DIR=/run/user/$(id -u $USER)

export THEME="$XDG_DATA_HOME/themes/colorscheme"

### panel (lemonbar + script) stuff
export PANEL_FIFO="$XDG_RUNTIME_DIR/panel-fifo"
[[ -p "$PANEL_FIFO" ]] && rm "$PANEL_FIFO"
mkfifo "$PANEL_FIFO"
export PANEL_PID="$XDG_RUNTIME_DIR/panel.pid"

### xob stuff
export XOB_FIFO="$XDG_RUNTIME_DIR/xob-fifo"
[[ -p "$XOB_FIFO" ]] && rm "$XOB_FIFO"
mkfifo "$XOB_FIFO"

export DUNST_DATA="$XDG_DATA_HOME/dunst"
[[ ! -d "$XDG_DATA_HOME/dunst" ]] && mkdir -p "$XDG_DATA_HOME/dunst"

### Qt5 theming. gtk2 (with qt5-styleplugins) or qt5ct
export QT_QPA_PLATFORMTHEME=gtk2

### GTK settings
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export GTK_OVERLAY_SCROLLING=1
export GTKM_INSERT_EMOJI
# export GTK_MODULES=canberra-gtk-module

### git prompt options
export GIT_PS1_HIDE_IF_PWD_IGNORED=1
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWUPSTREAM="auto"
export GIT_PS1_STATESEPARATOR=" :: "
# export GIT_PS1_DESCRIBE_STYLE="branch"
# export GIT_PS1_SHOWCOLORHINTS=1

### Bash
export BASH_COMPLETION_USER_FILE="$XDG_CONFIG_HOME"/bash-completion/bash_completion

### X11
export XAUTHORITY="$XDG_DATA_HOME/X11/Xauthority"
export XINITRC="$XDG_CONFIG_HOME/X11/xinit/xinitrc"
# export XSERVERRC="$XDG_CONFIG_HOME/X11/xinit/xserverrc"

### fzf
FD_OPTIONS="--hidden --follow --ignore-file $XDG_DATA_HOME/fd_ignore"
export FZF_DEFAULT_COMMAND="fd --type file --type symlink $FD_OPTIONS"
export FZF_CTRL_T_COMMAND="fd $FD_OPTIONS"
# export FZF_ALT_C_COMMAND="fd --type directory $FD_OPTIONS"

### misc
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export BOGOFILTER_DIR="$XDG_DATA_HOME/bogofilter"
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export DUC_DATABASE="$XDG_DATA_HOME/duc.db"
export HTTPIE_CONFIG_DIR="$XDG_CONFIG_HOME"/httpie
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export MOZ_DBUS_REMOTE=1
export MOZ_USE_XINPUT2=1
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export RXVT_SOCKET="$XDG_RUNTIME_DIR"/urxvtd
export SQLITE_HISTORY="$XDG_DATA_HOME"/sqlite_history
export SUDO_ASKPASS=$HOME/.local/bin/scripts/rofi-askpass
export VIFM="XDG_CONFIG_HOME/vifm"
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo
# export IPYTHONDIR="$XDG_CONFIG_HOME"/ipython
# export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
