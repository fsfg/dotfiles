# Auto-completion
# ---------------
if [[ $- == *i* ]] && [[ -f /usr/share/fzf/completion.bash ]]; then
    . /usr/share/fzf/completion.bash 2> /dev/null
fi


# Key bindings
# ------------
if [[ -f /usr/share/fzf/key-bindings.bash ]]; then
    . /usr/share/fzf/key-bindings.bash
fi
