#!/usr/bin/env bash

extract() {
    local c e i

    (($#)) || return

    for i; do
        c='' ; e=1

        if [[ ! -r $i ]]; then
            echo "$0: file is unreadable: \`$i'" >&2
            continue
        fi

        case $i in
            *.t@(gz|lz|xz|b@(2|z?(2))|a@(z|r?(.@(Z|bz?(2)|gz|lzma|xz)))))
                c=(bsdtar xvf) ;;
            *.7z)
                c=(7z x) ;;
            *.Z)
                c=(uncompress) ;;
            *.bz2)
                c=(bunzip2) ;;
            *.exe)
                c=(cabextract) ;;
            *.gz)
                c=(gunzip) ;;
            *.rar)
                c=(unrar x) ;;
            *.xz)
                c=(unxz) ;;
            *.zip)
                c=(unzip) ;;
            *)
                echo "$0: unrecognized file extension: \`$i'" >&2
                continue ;;
        esac

        command "${c[@]}" "$i"
        ((e = e || $?))
    done
    return "$e"
}


mkcd () {
    if [[ "$#" != 1 ]]; then
        echo "usage: mkcd <dir>"
    else
        mkdir -p "$1" && cd "$1"
    fi
}


bak() {
    local path basename_ dt
    path="$HOME/.backups$(pwd)/"
    [[ ! -d $path ]] && mkdir -p "$path"
    basename_="$(basename "$1")"
    dt="$(date +%Y%m%d%H%M%S)"
    cp "$1" "$path$basename_-$dt.backup"
}


#poetry() {
#    local poetry_bin="$(which poetry)";
#local poetry_workdir
#    if [[ $(find-back 'pyproject.toml' | wc -l) -ne 0 ]]; then
#        poetry_workdir="$(pwd)";
#    else
#        poetry_workdir="$HOME/Documents/dev/";
#    fi
#    cd "$poetry_workdir" && \
#    printf 'POETRY_WORKDIR: %s\n' "${poetry_workdir}" && \
#    $poetry_bin "$@" && \
#    cd - >"/dev/null"
#}



# vim: ft=sh
