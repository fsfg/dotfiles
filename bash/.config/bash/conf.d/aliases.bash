alias ls="ls --color=auto --group-directories-first \
	--human-readable  -l --literal"
alias la="ls --almost-all --color=auto --group-directories-first \
	--human-readable --indicator-style=slash -l --literal"
alias lsd='ls --color=auto -d */'
alias lad='ls --almost-all --color=auto --human-readable -l --classify -d *'

#alias docker-compose=podman-compose



# vim: ft=sh
