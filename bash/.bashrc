[[ $- != *i* ]] && return

_fg234="\[$(tput setaf 234)\]"
_bg2="\[$(tput setab 2)\]"
_bg3="\[$(tput setab 3)\]"
_fg5="\[$(tput setaf 5)\]"
_fgbold="\[$(tput bold)\]"
_res="\[$(tput sgr0)\]"

pre_prompt () {
    local color reset dt
    color="$(tput setaf 241)"
    reset="$(tput sgr0)"
    dt="  >> $(date +%H:%M:%S)"
    printf "%${COLUMNS}s\n" "${color}${dt}${reset}"
}
PROMPT_COMMAND=pre_prompt

title_="\[\e]2;[@\H]:\w - Terminal\a\]"
pwd_="${_fg234}${_bg2}  \w ${_res}"
git_status="${_fg234}${_bg3}\$(__git_ps1 '  %s ')${_res}"
prompt_="${_fgbold}${_fg5}>${_res} "

PS1="${title_}${pwd_}${git_status}\n\
${prompt_}"
PS2="${prompt_}"


export HISTFILE="$XDG_DATA_HOME"/bash/history
export HISTCONTROL=ignoredups:ignorespace:erasedups
export HISTIGNORE="&:cd:..:...:ls:la:lad:lsd:[bf]g:exit:yy:j"
export HISTSIZE=1024
export HISTFILESIZE=2048
history -a

bind "set show-all-if-ambiguous On"

# set -o vi
set -o noclobber

shopt -s autocd
shopt -s extglob
shopt -s dotglob
shopt -s nullglob
shopt -s globstar
shopt -s histappend
shopt -s cdspell
shopt -s dirspell
shopt -s checkjobs
shopt -s no_empty_cmd_completion

[[ $DISPLAY ]] && shopt -s checkwinsize
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


load_configs() {
  if [[ -z $1 ]]; then return 0; else config_dir="$1"; fi
  ext="${2:-''}"
  if test -d "$XDG_CONFIG_HOME/$config_dir"; then
    for conf in "$XDG_CONFIG_HOME/$config_dir"/*"$ext"; do
      test -r "$conf" && source "$conf";
    done
  fi
}


[[ -s /etc/profile.d/autojump.sh ]] && . /etc/profile.d/autojump.sh

if [[ -f /usr/share/doc/pkgfile/command-not-found.bash ]]; then
    . /usr/share/doc/pkgfile/command-not-found.bash
fi

if [[ -f /usr/share/git/completion/git-completion.bash ]]; then
    . /usr/share/git/completion/git-completion.bash
fi

if [[ -f /usr/share/git/completion/git-prompt.sh ]]; then
    . /usr/share/git/completion/git-prompt.sh
fi


# load common shell functions and aliases
load_configs 'common_posix_shell' 'sh'

# Load configs from $XDG_CONFIG_HOME/bash/conf.d
load_configs 'bash/conf.d' '.bash'

# Load completions from $XDG_CONFIG_HOME/bash/completion.d
load_configs 'bash/completion.d'


### pip bash completion
_pip_completion()
{
    COMPREPLY=( $( COMP_WORDS="${COMP_WORDS[*]}" \
                   COMP_CWORD=$COMP_CWORD \
                   PIP_AUTO_COMPLETE=1 $1 ) )
}
complete -o default -F _pip_completion pip


. /opt/asdf-vm/asdf.sh


# vim: ft=sh
