#!/usr/bin/env bash


checkdep() {
    for cmd in "$@"; do
        command -v "$cmd" > /dev/null || \
            { echo "Missing dependency: $cmd"; exit 1; }
    done
}
checkdep nmcli dunstify rofi

# nmcli device wifi rescan 2> /dev/null
dunstify -a "NetworkManager" -u low "NetworkManager" "Scanning wifi..."

# FIELDS=IN-USE,SSID,RATE,SIGNAL,BARS,SECURITY
# LIST="$(nmcli --terse --fields $FIELDS device wifi list)"
LIST="$(nmcli -g IN-USE,SSID device wifi list)"

ssid=$(rofi -pid "$XDG_RUNTIME_DIR/rofi.pid" \
            -config ~/.config/rofi/wifi.rasi \
            -disable-history \
            -i \
            -mesg "<b>Select WiFi SSID</b>" \
            -markup-rows \
            -dmenu <<< $LIST | cut -d ':' -f 2)

[ -z "$ssid" ] && exit 1

password=$(rofi -pid "$XDG_RUNTIME_DIR/rofi.pid" \
            -config ~/.config/rofi/prompt.rasi \
            -disable-history \
            -i \
            -mesg "<b>Enter password for $ssid:</b>" \
            -markup-rows \
            -password \
            -dmenu)

if [ -z "$password" ]; then
    nmcli dev wifi connect "$ssid"
else
    nmcli dev wifi connect "$ssid" password "$password"
fi


# def get_connections():
#     columns = ['NAME', 'UUID', 'TYPE', 'TIMESTAMP', 'DEVICE', 'STATE']
#     resp = run(['nmcli', '-g', ','.join(columns), 'con', 'show'], capture_output=True)
#     con = {'connections': []}

#     for el in [x.split(':') for x in resp.stdout.decode().split('\n') if x]:
#         con['connections'].append({k.lower():v if v else None for (k, v) in zip(columns, el)})

#     for c in con['connections']:
#         c['timestamp'] = datetime.fromtimestamp(int(c['timestamp']))

#     return con
