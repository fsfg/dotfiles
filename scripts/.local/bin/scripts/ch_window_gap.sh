#!/usr/bin/env bash

base_gap="$(bspc config window_gap)"
gap="$(("$1 + $base_gap"))"
min_gap=0
max_gap=55

[[ -z "$gap" ]] || [[ "$gap" -lt "$min_gap" ]] || [[ "$gap" -gt "$max_gap" ]] \
    && exit 0

sleep 0.05
bspc config window_gap "$gap"
