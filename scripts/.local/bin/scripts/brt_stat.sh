#!/usr/bin/env bash

### Check for dependencies.  Fail if unmet.
checkdep() {
    for cmd in "$@"; do
        command -v "$cmd" > /dev/null || \
            { echo "Missing dependency: $cmd"; exit 1; }
    done
}

checkdep light dunstify redshift

I_BRT="$(echo -e '')"

get_brightness() {
    local brightness_
	# brightness_="$(echo $(redshift -p 2>&1 | grep Brightness | \
	#    cut --delimiter=" " --fields=2) "* 100" | \
	#    bc -l | sed '/\./ s/\.\{0,1\}0\{1,\}$//')"
	# brightness_="$(xbacklight -get)"
    brightness_="$(light | xargs printf '%.0f')"
	printf '%s' "$brightness_"

}

get_period() {
    printf '%s' "$(redshift -p 2>/dev/null | grep Period | cut -d " " -f 2 | \
        awk '{print tolower($0)}')"
}

case "$1" in
    "show" )
    	dunstify --appname="redshift" --replace=999 --urgency=low --icon=" " \
            "$I_BRT Brightness: $(get_brightness)%" &
    	# exit 0
        ;;
    "check" )
        printf 'B%s\n' "$(get_period)"
        ;;
esac
