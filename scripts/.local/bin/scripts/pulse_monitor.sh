#!/usr/bin/env bash


checkdep() {
    for cmd in "$@"; do
        command -v "$cmd" > /dev/null || \
            { echo "Missing dependency: $cmd"; exit 1; }
    done
}

checkdep pactl

get_volume() {
    local raw volume is_muted
    raw=$(amixer get Master | grep '%' | head -n 1 | cut -d " " -f 7,8);
    volume="$(printf '%s' "$raw" | cut --delimiter=" " --fields=1)"
    is_muted="$(printf '%s' "$raw" | cut --delimiter=" " --fields=2)"
    [[ "${is_muted:1:-1}" == "off" ]] && is_muted="!" || is_muted=

    printf '%s' "${volume:1:-2} ${is_muted}"
}

pulse_mon() {
    declare -a vol arr
    local prev prev_m line current muted
    read -r -a vol < <(get_volume)
    prev="${vol[0]}"; prev_m="${vol[1]}"
    while read -r line ; do
        read -r -a arr <<< "$line"
        if [ "${arr[1]:1:-1}" == "change" ] && [ "${arr[3]}" == "sink" ]; then
            read -r -a vol < <(get_volume)
            current="${vol[0]}"; muted="${vol[1]}"
            if [[ "$current" != "$prev" ]] || [[ "$muted" != "$prev_m" ]]; then
                printf 'V%s\n' "${current}:${vol[1]}" > "$PANEL_FIFO"
                printf '%s\n' "${current}${vol[1]}" > "$XOB_FIFO"
                prev="$current"; prev_m="$muted"
            fi
        fi
    done
}

if [[ "$1" == "check" ]]; then
    read -r -a arr < <(get_volume)
    printf 'V%s\n' "${arr[0]}:${arr[1]}"
    exit 0
fi

# pgrep --full pulse_monitor.sh >&/dev/null && pkill --full pulse_monitor.sh
pgrep --exact pactl >&/dev/null && pkill --exact pactl
trap "trap - SIGTERM; kill 0" SIGINT SIGTERM SIGQUIT

pactl subscribe | pulse_mon

# getdefaultsinkname() {
#     pacmd stat | awk -F": " '/^Default sink name: /{print $2}'
# }

# getdefaultsinkvol() {
#     pacmd list-sinks |
#         awk '/^\s+name: /{indefault = $2 == "<'$(getdefaultsinkname)'>"}
#             /^\s+volume: / && indefault {print $5; exit}'

# }

# setdefaulsinkvol() {
#     pactl $(getdefaultsinkname) $1
# }
