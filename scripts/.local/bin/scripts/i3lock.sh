#!/usr/bin/env bash

### requires i3lock-color
### Check for dependencies.  Fail if unmet.
checkdep() {
    command -v "$1" > /dev/null || { echo "Missing dependency: $1"; exit 1; }
}

checkdep i3lock

[ -e "$THEME" ] && . "$THEME" || { echo "Missing THEME"; }

background="$color0"
background_alt="$color237"
foreground="${color15}ff"
font="mononoki"

i3lock \
    --color="$background" \
    --ignore-empty-password \
 	--show-failed-attempts \
    --bar-indicator \
    --bar-color="${background_alt}FF" \
    --clock \
    --datecolor="$foreground" \
    --date-font="$font" \
    --datesize=32 \
    --pass-media-keys \
    --time-font="$font" \
    --timecolor="$foreground" \
    --timesize=256 \
    --timestr="%H:%M"
