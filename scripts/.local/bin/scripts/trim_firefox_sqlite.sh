#!/usr/bin/env bash
# Firefox shell script: optimize SQLite by using vacuum
#

killall -q firefox
find "${HOME}/.config/mozilla" -name "*.sqlite" -exec sqlite3 '{}' vacuum \;
