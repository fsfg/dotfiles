#!/usr/bin/env bash


readonly sxhkd_config="$XDG_CONFIG_HOME/sxhkd/sxhkdrc"

cat "$sxhkd_config" \
    | awk '/^[a-z]/ && last {print $0,"\t",last} {last=""} /^#/{last=$0}' \
    | column -t -s $'\t' \
    | rofi \
        -disable-history \
        -dmenu \
        -i \
        -markup-rows \
        -mesg "Key bindings" \
        -pid "$XDG_RUNTIME_DIR/rofi.pid" > /dev/null
