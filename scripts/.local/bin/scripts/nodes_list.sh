#!/usr/bin/env bash


checkdep() {
    for cmd in "$@"; do
        command -v "$cmd" > /dev/null || \
            { echo "Missing dependency: $cmd"; exit 1; }
    done
}
checkdep bspc rofi


focus_node() {
    local _ids nodes selected_node
    declare -a nodes_ids

    if [[ "$1" == "--local" ]]; then
        _ids="$(bspc query -N --node .window -d "$(bspc query -D -d .active)")"
    else
        _ids="$(bspc query -N --node .window)"
    fi

    mapfile -t nodes_ids <<< "$_ids"
    nodes="$(xargs --no-run-if-empty xtitle <<< "${nodes_ids[@]}")"

    selected_node="$(rofi \
            -config ~/.config/rofi/window.rasi\
            -disable-history \
            -dmenu \
            -format 'i' \
            -i \
            -markup-rows \
            -mesg "<b>Select node</b>" \
            -pid "$XDG_RUNTIME_DIR/rofi.pid" \
                <<< "$nodes")"

    [ -z "$selected_node" ] && exit 1

    bspc node "${nodes_ids[$selected_node]}" --focus
}


case "$1" in
    "-l"|"--local" )
        focus_node --local ;;
    "-a"|"--all"|"" )
        focus_node ;;
esac
