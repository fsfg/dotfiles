#!/usr/bin/env bash

### Check for dependencies.  Fail if unmet.
checkdep() {
    command -v "$1" > /dev/null || { echo "Missing dependency: $1"; exit 1; }
}

checkdep rofi

OPTIONS=" Lock| Logout| Reboot|襤 Poweroff|鈴 Hibernate"

OUT="$(echo -e "$OPTIONS" | \
    rofi \
        -config ~/.config/rofi/powermenu.rasi \
        -disable-history \
        -format 'i' \
        -i \
        -markup-rows \
        -tokenize \
        -sep '|' \
        -dmenu -p\
    )"

case "$OUT" in
    0 )
        i3lock.sh          # ; sleep 60 && xset dpms force suspend
        ;;
    1 )
        pkill --full keeper &
        pkill --exact perWindowLayoutD &
        pkill --exact sxhkd &
        pkill --exact xob &
        pkill --exact greenclip &
        skippy-xd --stop-daemon &
        pkill --full nm_monitor.sh &
        pkill --full pulse_monitor.sh &
        pkill --full disk_monitor.sh &
        pokoy -k &
        pkill --full bar.sh &
        pkill --exact stalonetray &
        pkill --exact xsettingsd &
        bspc quit
        ;;
    2 )
        exec systemctl reboot
        ;;
    3 )
        exec systemctl poweroff
        ;;
    4 )
        systemctl hybrid-sleep
        ;;
esac
