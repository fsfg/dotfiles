convert_xres_to_ini() {
  sed -r 's/\*\.+//g; s/\:+/\=/g; s/[[:blank:]]//g'
}

PATTERN='-e color -e background -e foreground -e cursorColor'

convert_xres_to_ini < <(xrdb -query | grep $PATTERN) >| "$THEME"
