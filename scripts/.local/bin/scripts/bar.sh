#!/usr/bin/env bash

readonly NL="/dev/null"

### Check for dependencies.  Fail if unmet.
checkdep_hard() {
	for cmd in "$@"; do
		command -v "$cmd" > "$NL" \
			|| { echo "Missing dependency: $cmd"; exit 1; }
	done
}

checkdep_soft() {
	for cmd in "$@"; do
    	hash "$cmd" >"$NL" 2>&1 || { return 1; }
    done
}

checkdep_hard bspwm lemonbar pkill xdo

### GENERAL SETTINGS ###
### ----------------
readonly TERM_="st"
readonly PANEL_WM_NAME="bspwm_panel"
PANEL_PID="${PANEL_PID:=/tmp/panel.pid}"
PANEL_FIFO="${PANEL_FIFO:=/tmp/panel-fifo}"

if xdo id -a "$PANEL_WM_NAME" >&$NL ; then
	printf "%s\n" "The panel is already running."
	exit 1
fi
pgrep -x lemonbar >&$NL && pkill -x lemonbar

echo "$$" > "$PANEL_PID"

# gid="$(perl -l -0777 -ne '@f = /\(.*\)|\S+/g; print $f[4]' /proc/$$/stat)"

### Source the colour definitions if the file exists.
[ -e "$THEME" ] && . "$THEME" || { printf '%s' "Missing theme $THEME." >&2; }

### Colors
readonly BG_PANEL="$background"
readonly FG_PANEL="$color248"
readonly FG_ALT="$color245"
readonly BG_DSKTP="$color4"
readonly BG_DSKTP_ALT="$color12"
readonly FG_DSKTP="$color0"
readonly BG_MDL="$color6"
readonly FG_MDL="$color0"
readonly BG_WRNG="$color1"
readonly BG_LN="$color11"
readonly FG_ICN_MDL="$color237"
readonly FG_ICN_PERIOD="$color241"
readonly FG_ALERT="$color1"

readonly SP1="%{O5}"
readonly SP2="%{B$BG_PANEL}%{O1}%{B-}"

### Fonts
readonly FONT="mononoki Nerd Font Mono:style=Regular:size=9"
readonly FONT_ICON="mononoki Nerd Font Mono:style=Bold:size=9"
readonly FONT_NOTMONO="Lucida Grande:style=Regular:size=9"
# "PT Sans Caption:size=7"
# "Material Design Icons:style=Regular:size=9"
# "Symbola:style=Regular:size=7"


### Icons
readonly I_CLCK="%{T2}%{F$FG_ICN_MDL}$(echo -e '')%{F-}%{T-}"
readonly I_CAL="%{T2}%{F$FG_ICN_MDL}$(echo -e '')%{F-}%{T-}"
readonly I_LAYOUT_TILED="%{T2}$(echo -e 'ﱡ')%{T-}"
readonly I_LAYOUT_MONOCLE="%{T2}$(echo -e 'ﱢ')%{T-}"
readonly I_NODES="%{T2}$(echo -e '﮴')%{T-}"
readonly I_NODE_TILED="%{T2}$(echo -e '')%{T-}"
readonly I_NODE_PTILED="%{T2}$(echo -e '')%{T-}"
readonly I_NODE_FLOAT="%{T2}$(echo -e '穀')%{T-}"
readonly I_NODE_FLSCRN="%{T2}$(echo -e '')%{T-}"
readonly I_NODE_AT="%{T2}$(echo -e '')%{T-}"
readonly I_DSK="%{T2}%{F$FG_ALERT}$(echo -e '')%{F-}%{T-}"
readonly I_PUPD="%{T2}%{F$color3}$(echo -e '')%{F-}%{T-}"
readonly I_KBD="%{T2}%{F$FG_ICN_MDL}$(echo -e '')%{F-}%{T-}"
readonly I_WIFI_ON="%{T2}%{F$FG_ICN_MDL}$(echo -e '直')%{F-}%{T-}"
readonly I_WIFI_OFF="%{T2}%{F$FG_ICN_MDL}$(echo -e '睊')%{F-}%{T-}"
readonly I_VOL_OFF="%{T2}%{F$FG_ICN_MDL}$(echo -e 'ﱝ')%{F-}%{T-}"
readonly I_VOL_MUTE="%{T2}%{F$FG_ALERT}$(echo -e '婢')%{F-}%{T-}"
readonly I_VOL_1="%{T2}%{F$FG_ICN_MDL}$(echo -e '奄')%{F-}%{T-}"
readonly I_VOL_2="%{T2}%{F$FG_ICN_MDL}$(echo -e '奔')%{F-}%{T-}"
readonly I_VOL_3="%{T2}%{F$FG_ICN_MDL}$(echo -e '墳')%{F-}%{T-}"
readonly I_PERIOD_DAY="%{T2}%{F$FG_ICN_PERIOD}$(echo -e '')%{F-}%{T-}"
readonly I_PERIOD_TRANS="%{T2}%{F$FG_ICN_PERIOD}$(echo -e '')%{F-}%{T-}"
readonly I_PERIOD_NIGHT="%{T2}%{F$FG_ICN_PERIOD}$(echo -e '')%{F-}%{T-}"
readonly I_PERIOD_NONE="%{T2}%{F$FG_ALERT}$(echo -e '')%{F-}%{T-}"
gen_desktop_icon() {
	local desktop_icon n
	desktop_icon="$1"; n="$1"
	case "$n" in
		1) desktop_icon="%{T2}$(echo -e '1:爵')%{T-}" ;;
		2) desktop_icon="%{T2}$(echo -e '2:')%{T-}" ;;
		3) desktop_icon="%{T2}$(echo -e '3:')%{T-}" ;;
		4) desktop_icon="%{T2}$(echo -e '4:')%{T-}" ;;
		5) desktop_icon="%{T2}$(echo -e '5:')%{T-}" ;;
		6) desktop_icon="%{T2}$(echo -e '6:')%{T-}" ;;
		7) desktop_icon="%{T2}$(echo -e '7:_')%{T-}" ;;
		8) desktop_icon="%{T2}$(echo -e '8:_')%{T-}" ;;
		9) desktop_icon="%{T2}$(echo -e '9:_')%{T-}" ;;
		10) desktop_icon="%{T2}$(echo -e '10:_')%{T-}" ;;
	esac
	printf '%s' "$desktop_icon"
}

### Geometry
readonly PANEL_WIDTH=
readonly PANEL_HEIGHT=18
readonly PANEL_X=
readonly PANEL_Y=0
GEOMETRY="$PANEL_WIDTH"x"$PANEL_HEIGHT"+"$PANEL_X"+"$PANEL_Y"

bspc config top_padding "$PANEL_HEIGHT"

### Monitors
mapfile -t _monitors < <(bspc query -M --names)
declare -A MONS
for i in ${!_monitors[*]}; do
	MONS[${_monitors[$i]}]=$i
done
MON_QTY=${#MONS[*]}


### MODULES ###
# ----------------

### Date
dat() {
	declare -a _d
	while true; do
		read -r -a _d < <(date '+%a %b %-d %-H %M')
		printf 'D%s\n' "${_d[0]},%{O1}${_d[1]}%{O3}${_d[2]}"
    	printf 'C%s\n' "${_d[3]}:${_d[4]}"
		sleep 5
	done
}
dat > "$PANEL_FIFO" &

### Pacman updates
upd() {
	while true; do
		yupdate --check 2>"$NL" | wc -l | xargs printf 'U%s\n' 2>"$NL"
		sleep 3600
	done
}
(sleep 13 && upd > "$PANEL_FIFO") &

# X11 keyboard layout
kbd_() {
	checkdep_soft xkb-switch || return 1
	xkb-switch | xargs printf 'K%s\n'
	while read -r line < <(xkb-switch -W); do
		printf 'K%s\n' $line;
	done
}
kbd_ > "$PANEL_FIFO" &

### WM state
pgrep -x bspc >&$NL && pkill -x bspc
bspc subscribe report > "$PANEL_FIFO" &

### Windows' titlessleep 13 && upd > "$PANEL_FIFO"
pgrep -x xtitle >&$NL && pkill -x xtitle
xtitle -sf 'T%s\n' > "$PANEL_FIFO" &

nm_monitor.sh check > "$PANEL_FIFO" &
pulse_monitor.sh check > "$PANEL_FIFO" &
brt_stat.sh check > "$PANEL_FIFO" &


### BAR FUNCTION ###
# ----------------
bar() {
	focusd_cmd='bspc desktop --focus'
	while read -r line ; do
		case $line in
			### BSPWM's state
			W*)
				wm=("" ""); layout=("" ""); node_state=("" ""); flags=("" "")
				IFS=':'
				set -- ${line#?}
				while [ "$#" -gt 0 ] ; do
					item="$1"
					name="${item#?}"
					case "$item" in
						[Mm]*)
							mon="$name"
							mon_n="${MONS[$mon]}"
							mon_f=
							case "$item" in
								M*)
									bg_mon=$BG_DSKTP_ALT
									act_mon_bool=1
									act_mon="$mon_n"
									;;
								m*)
									bg_mon=$BG_DSKTP
									act_mon_bool=
									;;
							esac
							[ "$MON_QTY" -lt 2 ] && shift && continue
							mon_f="%{B$bg_mon}%{F$FG_DSKTP}\
								%{A1:bspc monitor --focus $mon:}\
								%{O2}${mon::-2}%{O2}\
								%{A}%{F-}%{B-}"
							;;
						F*)
							wm["$mon_n"]="\
								${wm["$mon_n"]}%{B$BG_DSKTP_ALT}%{F$FG_DSKTP}\
								%{A1:${focusd_cmd} ${name}:}\
								$SP1$(gen_desktop_icon "$name")$SP1\
								%{A}%{F-}%{B-}"
							;;
						O*)
							wm["$mon_n"]="\
								${wm["$mon_n"]}%{B$BG_DSKTP_ALT}%{F$FG_DSKTP}\
								%{A1:${focusd_cmd} ${name}:}\
								$SP1$(gen_desktop_icon "$name")$SP1\
								%{A}%{F-}%{B-}"
							;;
						o*)
							wm["$mon_n"]="\
								${wm["$mon_n"]}%{B$BG_DSKTP}%{F$FG_DSKTP}\
								%{A1:${focusd_cmd} ${name}:}\
								$SP1$(gen_desktop_icon "$name")%{U-}$SP1\
								%{A}%{F-}%{B-}"
							;;
						U*)
							wm["$mon_n"]="\
								${wm["$mon_n"]}%{B$BG_DSKTP_ALT}%{F$FG_DSKTP}\
								%{A1:${focusd_cmd} ${name}:}\
								$SP1%{U$BG_LN}%{+u}%{+o}\
								$(gen_desktop_icon "$name")\
								%{-u}%{-o}%{U-}$SP1%{A}%{F-}%{B-}"
							;;
						u*)
							wm["$mon_n"]="\
								${wm["$mon_n"]}%{B$BG_DSKTP}%{F$FG_DSKTP}\
								%{A1:${focusd_cmd} ${name}:}$SP1\
								%{U$BG_LN}%{+u}%{+o}\
								$(gen_desktop_icon "$name")\
								%{-u}%{-o}%{U-}$SP1%{A}%{F-}%{B-}"
							;;
						L*)
							case "${name}" in
								T) layout_icon="$I_LAYOUT_TILED" ;;
								M) layout_icon="$I_LAYOUT_MONOCLE" ;;
							esac
							layout["$mon_n"]="%{B$bg_mon}%{F$FG_DSKTP}\
								%{A1:bspc desktop --layout next:}\
								%{O3}${layout_icon}%{O3}\
								%{A}%{F-}%{B-}$mon_f$SP2"
							;;
						T*)
							[[ "$act_mon_bool" -eq 0 ]] && shift && continue
							case "$name" in
								T) node_state_icon="$I_NODE_TILED" ;;
								P) node_state_icon="$I_NODE_PTILED" ;;
								F) node_state_icon="$I_NODE_FLOAT" ;;
								=) node_state_icon="$I_NODE_FLSCRN" ;;
								@) node_state_icon="$I_NODE_AT" ;;
							esac
							node_state["$mon_n"]="%{O5}${node_state_icon}"
							;;
						G*)
							[[ "$act_mon_bool" -eq 0 ]] && shift && continue
							if [[ -n "${name}" ]]; then
								flags["$mon_n"]="%{O3}($name)"
							else
								flags["$mon_n"]=
							fi
							;;
					esac
					shift
				done

				nodes_count="$(bspc query -N -n .local.window | wc -l)"
				[[ $nodes_count -gt 0 ]] || nodes_count=
					nodes_="%{O3}%{F$FG_ALT}\
					%{A1:nodes_list.sh --all:}\
					%{A3:nodes_list.sh --local:}\
					${I_NODES}%{O1}${nodes_count}\
					%{A}%{A}%{F-}%{O3}"
				;;
			### Window title
			T*)
				title="%{O5}${line#?}"
				declare -a title
				for m in ${MONS[*]}; do
					if [[ "$m" == "$act_mon" ]]; then
						title[m]="%{O5}%{T1}${line#?}%{T-}"
					else
						title[m]=
					fi
				done
				;;
			### High disk usage warning
			d*)
				dsk="${line#?}"
				if [[ -n "$dsk" ]]; then
					du="%{B$BG_WRNG}\
						%{A1:disk_monitor.sh --show > $PANEL_FIFO &:}\
						%{A3:duc index --one-file-system --quiet ~ && \
							duc gui --dark --gradient --ring-gap=5 &:}\
						%{O5}$I_DSK%{O3}$dsk%%{O5}\
						%{A}%{A}%{B-}"
				else
					du=
				fi
				;;
			### Pacman updates
			U*)
				updn="${line#?}"
				if [[ "$updn" -ne 0 ]]; then
					pupd="\
						%{A1:$TERM_ -n YUpdate -t 'YUpdate' \
                            -e yupdate --full >&$NL &:}\
						%{O5}$I_PUPD%{O3}${updn}%{O5}\
						%{A}$SP2"
					dunstify --appname="yupdate" "YUpdate" \
						"Updates available ($updn)"
				else
					pupd=
				fi
				;;
			### Wireless[/wired] network
			N*)
				ssid=${line#?}
				[[ -n "$ssid" ]] && \
					ssidout="$I_WIFI_ON%{O3}%{F$FG_MDL}$ssid" \
						|| ssidout="$I_WIFI_OFF"
				net="%{B$BG_MDL}\
					%{A1:wifi.sh >&$NL &:}\
					%{A3:$TERM_ -n nmtui -e nmtui-connect >&$NL &:}\
					%{O5}$ssidout%{O5}\
					%{A}%{A}%{F-}%{B-}$SP2"
				;;
			### Volume
			V*)
				IFS=':'; read -r -a arr_vol <<< "${line#?}"; unset IFS;
				if [[ "${arr_vol[1]}" == "!" ]]; then
					icon="$I_VOL_MUTE"	#; vol_r="[Muted]"
				else
					vol_r="${arr_vol[0]}"
					if [[ "$vol_r" -eq 0 ]] ; then
						icon="$I_VOL_OFF"
					elif [[ "$vol_r" -lt 16 ]]; then
						icon="$I_VOL_1"
					elif [[ "$vol_r" -le 64 ]] ; then
						icon="$I_VOL_2"
					else
						icon="$I_VOL_3"
					fi
					if [[ "${vol_r}" -lt 10 ]]; then
						vol_r=" ${vol_r}%"
					else
						vol_r="${vol_r}%"
					fi
				fi
				vol="%{B$BG_MDL}\
					%{A1:volume.sh mute:}\
					%{A3:pavucontrol &:}\
					%{A4:volume.sh up:}\
					%{A5:volume.sh down:}\
					$SP1$icon%{O3}%{F$FG_MDL}$vol_r%$SP1\
					%{A}%{A}%{A}%{A}%{F-}%{B-}$SP2"
				;;
			# X11 keyboard layout
			K*)
				kbd_layout="%{B$BG_MDL}\
					%{A1:xkb-switch -n|--next:}\
					%{O5}$I_KBD%{O3}%{F$FG_MDL}${line#?}%{O5}\
					%{A}%{F-}%{B-}$SP2"
				;;
			### Date
			D*)
				dt="%{B$BG_MDL}\
					%{A1:gsimplecal &:}\
					%{O5}$I_CAL%{O3}%{F$FG_MDL}${line#?}%{O5}\
					%{A}%{F-}%{B-}$SP2"
				;;
			### Clock
			C*)
				clk="%{B$BG_MDL}\
					%{O5}$I_CLCK%{O3}%{F$FG_MDL}${line#?}%{O2}\
					%{F-}%{B-}"
				;;
			### Brightness
			B*)
				case "${line#?}" in
					daytime)
						_period="$I_PERIOD_DAY" ;;
					transition)
						_period="$I_PERIOD_TRANS" ;;
					night)
						_period="$I_PERIOD_NIGHT" ;;
					none)
						_period="$I_PERIOD_NONE" ;;
				esac
				brt="%{B$BG_MDL}\
					%{A1:pkill -USR1 redshift:}\
					%{A3:brt_stat.sh show:}\
					%{O5}$_period%{O5}\
					%{A}%{A}%{B-}"
				;;
		esac

		if [[ "$MON_QTY" -gt 1 ]]; then
			OUT_SEC_MON="\
				%{S1}\
					%{l}\
						${layout[1]}\
						${wm[1]}%{O5}\
						${node_state[1]}\
						${flags[1]}\
						${title[1]}\
				"
		else
			OUT_SEC_MON=
		fi

		printf '%s\n' "\
			%{S0}\
				%{l}\
					${layout[0]}\
					${wm[0]}%{O5}\
					${nodes_}\
					${node_state[0]}\
					${flags[0]}\
					${title[0]}\
				%{r}%{O5}\
					$du\
					$pupd\
					$kbd_layout\
					$vol\
					$net\
					$dt\
					$clk\
					$brt\
			$OUT_SEC_MON\
			"
	done
}


### FINAL OUTPUT ###
# ----------------

bar < "$PANEL_FIFO" \
	| lemonbar -p -a 32 -u 2 -n "$PANEL_WM_NAME" \
		-B "$BG_PANEL" \
		-F "$FG_PANEL" \
		-f "$FONT" -f "$FONT_ICON" -f "$FONT_NOTMONO" \
		-g "$GEOMETRY" \
	| sh &


### PANEL MAINTAINING ###
# ----------------

### Place wm above bar
wid=$(xdo id -a "$PANEL_WM_NAME")
tries_left=20

while [ -z "$wid" ] && [ "$tries_left" -gt 0 ] ; do
	sleep 0.05
	wid=$(xdo id -a "$PANEL_WM_NAME")
	tries_left=$((tries_left - 1))
done

wid="$(printf '%s' "$wid" | sort | head -n 1)"

[ -n "$wid" ] && \
	xdo above -t "$(xdo id -N Bspwm -n root | sort | head -n 1)" "$wid"

### Kill child processes on panel stop
# trap "trap - TERM; kill 0; pkill lemonbar" SIGINT SIGTERM SIGQUIT
trap "pkill lemonbar" SIGINT SIGTERM SIGQUIT

wait


# vim: ft=sh
