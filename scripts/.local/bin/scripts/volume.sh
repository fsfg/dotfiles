#!/usr/bin/env bash


checkdep() {
    command -v "$1" > /dev/null || { echo "Missing dependency: $1"; exit 1; }
}
checkdep pactl

SINK="0"

case "$1" in
	"up")
		pactl set-sink-mute "$SINK" 0
		pactl set-sink-volume "$SINK" +4%
		;;
	"down")
		pactl set-sink-volume "$SINK" -4%
		;;
	"mute")
		pactl set-sink-mute "$SINK" toggle
		;;
esac
