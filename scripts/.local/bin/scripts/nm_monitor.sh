#!/usr/bin/env bash


checkdep() {
    for cmd in "$@"; do
        command -v "$cmd" > /dev/null || \
            { echo "Missing dependency: $cmd"; exit 1; }
    done
}

checkdep nmcli dunstify pgrep

WIFI_IF="wls3"
I_WIFI_ON="$(echo -e '')"

get_ssid() {
    printf '%s' "$(nmcli --terse --fields GENERAL.CONNECTION \
        device show $WIFI_IF | cut --delimiter=':' --fields=2)"
}

nm_monitor() {
    local line arr
    while read -r line ; do
        read -r -a arr <<< "$line"
        case "${arr[0]}" in
            "${WIFI_IF}:" )
                case "${arr[1]}" in
                    "connected" )
                        printf 'N%s\n' "$(get_ssid)"
                        ;;
                    "disconnected" )
                        printf 'N%s\n' ""
                        ;;
                esac
                dunstify --appname="NetworkManager" --replace=997 \
                    --urgency=low --icon=" " "$I_WIFI_ON $line" &
                ;;
        esac
    done
}

if [[ "$1" == "check" ]]; then
    printf 'N%s\n' "$(get_ssid)"
    exit 0
fi

# pgrep --full nm_monitor.sh >&/dev/null && pkill --full nm_monitor.sh
pgrep --exact nmcli >&/dev/null && pkill --exact nmcli
trap "trap - SIGTERM; kill 0" SIGINT SIGTERM SIGQUIT

nmcli monitor | nm_monitor > "$PANEL_FIFO"
