#!/usr/bin/env bash

### To check usage for certain disk only use this command
### (change the "[ab]" with one needed)
### df -hl | awk '/^\/dev\/sd[ab]/ { sum+=$5 } END { print sum }'

### Check for dependencies.  Fail if unmet.
checkdep() {
    command -v "$1" > /dev/null || { echo "Missing dependency: $1"; exit 1; }
}

checkdep dunstify

readonly ID="998"
readonly MAX_DISK_USAGE=96
readonly I_DSK="$(echo -e '')"
readonly TIME_IN_BETWEEN=600

# Get % of disk usage
get_disk_usage() {
    printf '%s' "$(df -hl | awk '/^\/dev\/sd[a]/ { sum+=$5 } END { print sum }')"
}

check_critical() {
    local disk_usage out
    disk_usage="$(get_disk_usage)"
    case "$1" in
        "" )
            if [[ "$disk_usage" -gt "$MAX_DISK_USAGE" ]] ; then
                out="$disk_usage"
                dunstify --replace="$ID" --urgency=critical \
                    "Disk Usage High" "$I_DSK Reached ${disk_usage}%" &
            else
                out=
                dunstify --close="$ID"
            fi
            printf 'd%s\n' "$out"
            ;;
        "--verbose" )
            printf "%s\n" "$disk_usage"
            ;;
    esac
}

case "$1" in
    "-s"|"--show"|"show" )
        case "$2" in
            "" )
            check_critical
            ;;
            "-v"|"--verbose" )
                check_critical --verbose
                ;;
        esac
        ;;
    "-d"|"" )
        while true; do
            check_critical > "$PANEL_FIFO"
            sleep "$TIME_IN_BETWEEN"
        done
        ;;
esac
